# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Le Guennec, Yaakoub, email: yaakoub.leguennec@univ-lille.fr

- Gatari, Jean debout email: jeandebout.gatari@univ-lille.fr

## Question 1

Comme le EUID du fichier titi.txt correspond à l'utilisateur toto qui a lancé le fichier (via le processus) alors c'est le premier triplet
des droits d'accès que le système considerera en premier (et pas le groupe). Or le propriétaire du fichier n'a pas les droits d'écriture, donc le processus ne peut pas écrire dans le fichier. 

## Question 2

Quand l'utilisateur toto execute la commande 'cd myDir' il aura une erreur de permissions et ne peut pas executer la commande
sur ce répertoire. On peut donc supposer que la permission d'execution sur un répertoire correspond à la possibilité d'executer des commandes 
au sein du répertoire.

Quand on essaye de lister le contenu du répertoire avec la commande 'ls -al myDir' alors on aura une erreur de permission suite à la tentative
d'executer la commande ls sur les fichiers du répertoire. Ainsi on ne peut pas récuperer les droits d'accès des fichiers du répertoire myDir, mais seulement les noms des fichiers.


## Question 3

A l'execution du programme les différents id affichés correspondent à l'utilisateur toto, mais le processus ne peut pas lire le fichier data.txt avec ces droits actuels.
Quand on fait la commande 'chmod u+s a.out" sur le fichier executable alors on peut lire le fichier data.txt et le EUID a changé pour correspondre à l'utilisateur ubuntu, en revanche les autres id sont inchangés.

## Question 4

Malgré le fait qu'on ait activé le flag set-user-id, le script python affiche toto comme EUID.
L'utilisateur peut changer un de ses attributs en passant par un executable qui contient le flag set-user-id avec comme propriétaire
un utilisateur root.

## Question 5

La commande 'chfn' permet de modifier les informations d'un utilisateur, comme le nom d'utilisateur par exemple.
Résultat de la commande 'ls -al etc/bin/chfn' : 

-rwsr-xr-x  root root ... /usr/bin/chfn

Il est interessant de constater que la commande chfn contient le flag set-user-id avec comme propriétaire root et aussi que tout le 
monde peut l'executer. .

## Question 6

Les mots de passes des utilisateur sont stockés dans le fichier etc/shadow et contient le hachage du mot de passe crypté.
S'ils étaient stocké dans passwd on pourrait les modifier avec la commande chfn et meme les visualiser, c'est donc pour des raisons de sécurité.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








