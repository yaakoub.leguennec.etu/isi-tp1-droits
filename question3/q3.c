#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define FILE_LINK "myDir/data.txt"

int main(int argc, char *argv[]) {

	FILE *file;
	char buffer[1024];
	size_t nread;
	
	uid_t euid = geteuid();
	uid_t egid = getegid();
	uid_t ruid = getuid();
	uid_t rgid = getgid();

	printf("EUID : %u ", euid);
	printf("EGID : %u ", egid);
	printf("RUID : %u ", ruid);
	printf("RGID : %u \n", rgid);
	
	file = fopen(FILE_LINK, "r");
	
	if(file == NULL) {
		perror("Cant open file");
		exit(EXIT_FAILURE);
	} 
	
	printf("fichier ouvert!\n"); 
	
	while( (nread = fread(buffer, 1, sizeof buffer, file)) > 0) 
		printf("%s", buffer);
	
	
	fclose(file);

return 0;
}
